# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard[at]manjaro[dot]org>
# Maintainer: Helmut Stult <helmut[at]manjaro[dot]org>

# Arch credits:
# Maintainer : Thomas Baechler <thomas@archlinux.org>
# Contributor: Alonso Rodriguez <alonsorodi20 (at) gmail (dot) com>
# Contributor: Sven-Hendrik Haase <sh@lutzhaase.com>
# Contributor: Felix Yan <felixonmars@archlinux.org>
# Contributor: loqs
# Contributor: Dede Dindin Qudsy <xtrymind+gmail+com>
# Contributor: Ike Devolder <ike.devolder+gmail+com>

_linuxprefix=linux-lqx
_extramodules=extramodules-6.4-lqx
# don't edit here
pkgver=390.157_6.4.4.lqx1_1

_nver=390
# edit here for new version
_sver=157
# edit here for new build
pkgrel=1
pkgname=$_linuxprefix-nvidia-${_nver}xx
_pkgname=nvidia
_pkgver="${_nver}.${_sver}"
pkgdesc="NVIDIA drivers for linux."
arch=('x86_64')
url="http://www.nvidia.com/"
depends=("$_linuxprefix" "nvidia-390xx-utils=${_pkgver}")
makedepends=("$_linuxprefix-headers")
groups=("$_linuxprefix-extramodules")
provides=("nvidia=$_pkgver")
conflicts=("$_linuxprefix-nvidia-340xx" "$_linuxprefix-nvidia-418xx" "$_linuxprefix-nvidia-430xx"
           "$_linuxprefix-nvidia-435xx" "$_linuxprefix-nvidia-440xx" "$_linuxprefix-nvidia-450xx"
           "$_linuxprefix-nvidia-455xx" "$_linuxprefix-nvidia-460xx")
license=('custom')
install=nvidia.install
options=(!strip)
durl="http://us.download.nvidia.com/XFree86/Linux-x86"
source=("${durl}_64/${_pkgver}/NVIDIA-Linux-x86_64-${_pkgver}-no-compat32.run"
        'kernel-6.2.patch'
        'kernel-6.3.patch'
        'kernel-6.4.patch')
sha256sums=('162317a49aa5a521eb888ec12119bfe5a45cec4e8653efc575a2d04fb05bf581'
            '3501f0bbd9603543da74873905448ff58f5095948a7375617bba74f122d575aa'
            '4284f95f808df4e43afc4632e3fc1f87da1a805f0f6f9af1f6b519c7cf7562b4'
            '6a73ba0760c278a835ec5dcc6f3a9e0f8f0787fde95e832c38e8038152242708')

_pkg="NVIDIA-Linux-x86_64-${_pkgver}-no-compat32"

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2 | cut -f1-2 -d'-')
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
    sh "${_pkg}.run" --extract-only

    # patches here
    cd "${_pkg}/kernel"
    patch -Np1 -i ${srcdir}/kernel-6.2.patch
    patch -Np1 -i ${srcdir}/kernel-6.3.patch
    patch -Np1 -i ${srcdir}/kernel-6.4.patch
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
    cd "${_pkg}"
    make -C kernel SYSSRC=/usr/lib/modules/"${_kernver}/build" module
}

package() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    depends=("${_linuxprefix}=${_ver}")

    cd "${_pkg}"
    install -Dm644 kernel/*.ko -t "${pkgdir}/usr/lib/modules/${_extramodules}/"

    # compress each module individually
    find "${pkgdir}" -name '*.ko' -exec xz -T1 {} +

    sed -i -e "s/EXTRAMODULES='.*'/EXTRAMODULES='${_extramodules}'/" "${startdir}/nvidia.install"
}
